all:
	python setup.py build

install:
	python setup.py install

sdist:
	python setup.py sdist upload

clean:
	rm -fr build dist *.egg-info
